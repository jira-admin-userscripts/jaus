// ==UserScript==
// @name        JAUS Admin Check All
// @namespace   JAUS
// @version     1.1.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez)
// @description Detects checkboxes on Admin screens and injects a button to check/uncheck them all
// @homepage    https://jaus.atlassian.net/wiki/spaces/JAUS/pages/33224/Admin+Check+All
// @supportURL  https://jaus.atlassian.net/servicedesk/customer/portals
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// @match       *://*/*/AssociateFieldToScreens*
// @grant       GM_addStyle
// @downloadURL https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-admin-check-all.user.js
// @updateURL   https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-admin-check-all.user.js
// ==/UserScript==
let jausUserscriptVersion = 'v1.1.0'; // Always keep this in sync with the @version above

var jausCSS = `
.jaus {
    all: none;
    font-size: 14px;
    font-family: "Segoe UI", Helvetica, sans-serif, monospace;
}

.jaus-main-button {
    color: #555555;
    font-weight: bold;
    text-align: center;
    margin-left: 30px;
    border-style: solid;
    border-width: 1px;
    border-radius: 5px;
    border-color: GoldenRod;
    background-color: gold;
    padding: 4px 10px;
    cursor: pointer;
    transition-duration: 0.1s;
    transition-property: background-color;
    transition-timing-function: ease-out;
}

.jaus-main-button:hover {
    background-color: #ffe033;
}
`;

function jausPrintVersion() {
    return '<a href="https://jaus.atlassian.net/wiki/spaces/JAUS/pages/33224/Admin+Check+All" target="_blank">' + jausUserscriptVersion + '&nearr;</a>';
}

// Control Flag to check/uncheck the row of fields
let jausACAControlFlag = false;

/**
 * Checks/unchecks all boxes in the Custom Field Screen association screen.
 */
function adminCheckAllFieldScreens() {
  let boxes = document.getElementsByName('associatedScreens');
  if (boxes) {
    for (let i = 0; i < boxes.length; i++) {
      if (jausACAControlFlag)
        boxes[i].checked = ''
      else
        boxes[i].checked = 'checked';
    }
    jausACAControlFlag = !jausACAControlFlag;
  }
}

/*
 * Checks/clears the checboxes in the current screen.
 */
function jausAdminCheckAll() {
  adminCheckAllFieldScreens();
  // Add more functions that work for other screens/checkboxes
}

/*
 * "MAIN" BODY:
 * Injects a button into the top menu.
 */
setTimeout(function () {
    let jausCreateButtonDC = document.getElementById('create_link');
    if (jausCreateButtonDC) {
        GM_addStyle(jausCSS);
        let jausCreateButtonParent = jausCreateButtonDC.parentNode.parentNode;
        var jausNode = document.createElement("li");
        jausNode.id = 'jaus-aca-main-menu';
        //aui-button aui-button-primary aui-style
        jausNode.innerHTML = '<div class="jaus jaus-main-button" id="jaus-aca-main-button" accessKey="u" title="' + jausUserscriptVersion + '">Check All</div>';
        jausCreateButtonParent.appendChild(jausNode);
        let jausContextButton = document.getElementById("jaus-aca-main-button");
        jausContextButton.addEventListener("click", jausAdminCheckAll);
        window.JAUS = {};
        window.JAUS.isCloud = false;
    }
}, 1000);
