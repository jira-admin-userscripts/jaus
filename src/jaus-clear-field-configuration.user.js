// ==UserScript==
// @name        JAUS Clear Field Configuration
// @namespace   JAUS
// @version     1.1.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez)
// @description Hides or shows all fields from the Field Configuration.
// @homepage    https://jaus.atlassian.net/wiki/spaces/JAUS/pages/1441821/Clear+Field+Configuration
// @supportURL  https://jaus.atlassian.net/servicedesk/customer/portals
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// @match       *://*/*/ConfigureFieldLayout*
// @grant       GM_addStyle
// @grant       GM_xmlhttpRequest
// @downloadURL https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-clear-field-configuration.user.js
// @updateURL   https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-clear-field-configuration.user.js
// ==/UserScript==
let jausUserscriptVersion = 'v1.1.0'; // Always keep this in sync with the @version above

/*
 * Spinner from https://aui.atlassian.com/aui/7.9/docs/spinner.html
 * So using spinner from CSS from w3schools.com/howto/howto_css_loader.asp
 */
var jausSpinnerCSS = `
.jausSpinnerClass {
    visibility: hidden;
    margin-left: auto;
    margin-right: auto;
    margin-top: 10px;
    position: center;
    border: 4px solid #DFE1E6;
    border-top: 4px solid #0052CC;
    border-radius: 50%;
    width: 24px;
    height: 24px;
    animation: spin 2s linear infinite;
    text-align: center;
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}`;

var jausCSS = `
.jaus {
    all: none;
    font-size: 14px;
    font-family: "Segoe UI", Helvetica, sans-serif, monospace;
}

.jaus-main-button {
    color: #555555;
    font-weight: bold;
    text-align: center;
    margin-left: 30px;
    border-style: solid;
    border-width: 1px;
    border-radius: 5px;
    border-color: GoldenRod;
    background-color: gold;
    padding: 4px 10px;
    cursor: pointer;
    transition-duration: 0.1s;
    transition-property: background-color;
    transition-timing-function: ease-out;
}

.jaus-main-button:hover {
    background-color: #ffe033;
}

.jaus-blanket {
    background-color: rgba(23, 43, 77, 0.45);
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 2500;
    left: 0px;
    top: 0px;
}

.jaus-box {
    top: 100px;
    right: 0;
    bottom: 100px;
    left: 0;
    margin: auto;
    position: fixed;
    display: block;
    background-color: white;
    z-index: 3000;
    width: 35%;
    height: 370px;
    max-height: 50%;
    min-height: 300px;
    border-style: solid;
    border-color: white;
    border-radius: 5px;
    font-family: "Segoe UI", Helvetica, sans-serif, monospace;
}

.jaus-box header {
    position: sticky;
    height: 60px;
    display: flex;
    border-bottom-style: solid;
    border-bottom-width: 2px;
    border-bottom-color: #ebecf0;
}

.jaus-box header h1 {
    margin-left: 0px;
    text-align: left;
    font-size: 20px;
    position: sticky;
    color: rgb(23, 43, 77);
    padding: 10px 10px 10px 20px;
}

.jaus-box header nav {
    position: absolute;
    margin-left: auto;
    padding: 5px;
    right: 0px;
}

.jaus-box article {
    display: block;
    position: absolute;
    top: 62px;
    right: 0px;
    bottom: 52px;
    left: 0px;
    overflow: auto;
    padding: 30px;
}

.jaus-box article p {
    color: rgb(107, 119, 140);
    font-size: 14px;
    line-height: 20px;
    display: inline-block;
}

.jaus-box article label {
    font-weight: 500;
    width: 130px;
    text-align: right;
    display: inline-grid;
    padding: 3px 20px 3px 4px;
}

.jaus-cfc-field-feedback {
    font-weight: 500;
    width: 100px;
    height: 20px;
    display: inline;
    visibility: visible;
    padding: 3px 5px 3px 5px;
    margin-left: 5px;
    position: absolute;
    vertical-align: middle;
}

#jaus-cfc-rp-found {
    margin-left: 0px;
    position: absolute;
    visibility: hidden;
    vertical-align: middle;
    color: rgb(0, 135, 90);
}

#jaus-cfc-rp-not-found {
    margin-left: 0px;
    position: absolute;
    visibility: hidden;
    vertical-align: middle;
    color: rgb(255, 171, 0);
}

#jaus-cfc-search-button {
    margin-left: 10px;
}

.jaus-box footer {
    color: #555555;
    position: absolute;
    display: flex;
    bottom: 0px;
    right: 0px;
    left: 0px;
    height: 50px;
    border-top-style: solid;
    border-top-width: 2px;
    border-top-color: #ebecf0;
}

.jaus-box footer p {
    position: absolute;
    right: 0px;
    bottom: 0px;
    padding: 5px;
    font-style: italic;
    font-family: monospace, sans-serif, v, "Segoe UI";
    display: inline-grid;
}

.jaus-text-field {
    background-color: rgb(250, 251, 252);
    border-style: solid;
    border-width: 2px;
    border-color: rgb(223, 225, 230);
    font-size: 14px;
    padding: 3px 4px;
    width: 300px;
    line-height: 20px;
    border-radius: 3px;
    color: rgb(23, 43, 77);
    font-weight: 400;
    display: inline-grid;
}

.jaus-select-field {
    background-color: rgb(235, 236, 240);
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill='%23344563' d='M6.744 8.744a1.053 1.053 0 000 1.49l4.547 4.557a1 1 0 001.416 0l4.55-4.558a1.051 1.051 0 10-1.488-1.488l-3.77 3.776-3.768-3.776a1.051 1.051 0 00-1.487 0z'/%3E%3C/svg%3E");
    background-position: 100% 50%;
    background-repeat: no-repeat;
    border-style: solid;
    border-width: 2px;
    border-color: rgb(235, 236, 240);
    display: inline-grid;
    appearance: none;
    border-radius: 3px;
    font-weight: 400;
    font-size: 14px;
    color: rgb(23, 43, 77);
    height: 30px;
    padding: 3px 0px 3px 5px;
    width: 312px;
}

.jaus-cfc-lower-section {
    border-top-style: solid;
    border-top-width: 2px;
    border-top-color: #ebecf0;
    margin-top: 15px;
    width: 100%;
    position: relative;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
}

.jaus-cfc-search-feedback {
    font-weight: 500;
    width: 100%;
    height: 15px;
    padding: 3px 5px 3px 5px;
    position: relative;
    vertical-align: middle;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    margin-top: 10px;
}

#jaus-cfc-commit-button {
    margin-top: 10px;
    margin-left: 5px;
    padding: 3px 5px 3px 5px;
}

.jaus-cfc-warning {
    color: rgb(23, 43, 77);
    font-weight: 500;
    display: block;
    text-align: center;
    padding: 5px;
    color: gold;
    border-style: solid;
    border-color: gold;
}

#jaus-cfc-form-commit {
    margin-top: 30px;
    font-size: 30px;
}
`;

function jausPrintVersion() {
    return '<a href="https://jaus.atlassian.net/wiki/spaces/JAUS/pages/1441821/Clear+Field+Configuration" target="_blank">' + jausUserscriptVersion + '&nearr;</a>';
}

/**
 * Injects or removes the background "blanket", blurring everything else.
 * If the blanket is present, removes it — if it's not present, injects it.
 */
function jausToggleBlanket() {
    let jausBlanket = document.getElementById('jaus-cfc-blanket');
    if (jausBlanket) {
        jausBlanket.parentNode.removeChild(jausBlanket);
    } else {
        let blanket = document.createElement("div");
        blanket.id = "jaus-cfc-blanket";
        blanket.className = "jaus jaus-blanket";
        document.body.appendChild(blanket);
        blanket.addEventListener("click", jausToggleBlanket);
    }
}

function toggleJAUSBox() {
    let jausBox = document.getElementById('jaus-cfc-box');
    let jausBlanket = document.getElementById('jaus-cfc-blanket');
    let boxRemoved = false;
    if (jausBox) {
        jausBox.parentNode.removeChild(jausBox);
        boxRemoved = true;
    }
    if (jausBlanket) {
        jausBlanket.parentNode.removeChild(jausBlanket);
        boxRemoved = true;
    }
    if (!boxRemoved) {
        let jausBlanket = document.createElement("div");
        jausBlanket.id = "jaus-cfc-blanket";
        jausBlanket.className = "jaus jaus-blanket";
        document.body.appendChild(jausBlanket);
        jausBlanket.addEventListener("click", toggleJAUSBox);

        jausBox = document.createElement("section");
        jausBox.id = 'jaus-cfc-box';
        jausBox.className = 'jaus jaus-box';
        jausBox.innerHTML = '<header id="jaus-cfc-box-header"><h1>JAUS - Clear Field Configuration</h1><nav id="jaus-cfc-nav"><a href="#" id="jaus-cfc-close-button">Close</a></nav></header><article id="jaus-cfc-content"><section>' +
            '<aside class="jaus jaus-cfc-warning"><p><strong>Warning!</strong></p><br>' +
            '<p>Make sure the Field Configuration <strong>is not in use</strong>!</p></aside>' +
            '<form id="jaus-cfc-form-commit" method="POST" action="">' +
            '<p><label for="jaus-cfc-action">Action to perform </label><select class="jaus jaus-select-field" id="jaus-cfc-action" required><option value="hide" selected>Hide all fields</option><option value="show">Show all fields</option></select>' +

            '<button type="button" id="jaus-cfc-commit-button">Commit!</button></p>' +
            '<aside><div class="jaus jausSpinnerClass" id="jaus-cfc-spinner"></div></aside>' +
            '</form></section>' +
            '</article><footer id="jaus-box-footer"><p>' + jausPrintVersion() + '</p></footer>';
        document.body.appendChild(jausBox);
        document.getElementById('jaus-cfc-close-button').addEventListener("click", toggleJAUSBox);
        document.getElementById('jaus-cfc-commit-button').addEventListener("click", jausToggleAllFields);
    }
}

function jausToggleField(index) {
    if (index < window.JAUS.links.length) {
        GM_xmlhttpRequest({
            method: "GET",
            url: window.JAUS.links[index].href,
            onload: function (responseHide) {
                window.JAUS.progress = window.JAUS.progress + 1;
                jausToggleField(index + 1);
            }
        });
    } else {
        location.reload();
    }
}

function jausToggleAllFields() {
    document.getElementById('jaus-cfc-commit-button').style.visibility = "hidden";
    document.getElementById('jaus-cfc-spinner').style.visibility = "visible";
    if (document.getElementById('jaus-cfc-action').value == "hide") {
        let hideLinks = document.querySelectorAll('a[id^="hide_"]');
        if (hideLinks) {
            window.JAUS.links = hideLinks;
            jausToggleField(0);
        }
    } else {
        let showLinks = document.querySelectorAll('a[id^="show_"]');
        if (showLinks) {
            window.JAUS.links = showLinks;
            jausToggleField(0);
        }
    }
}

/**
 * Tries to detect if the Field Configuration is in use.
 */
function jausIsConfigInUse() {
    /* For Cloud and Server/DC */
    var emList = document.getElementsByTagName("em");
    if (emList) {
        for (let i = 0; i < emList.length; i++) {
            if (emList[i].innerHTML.toLowerCase().includes("shared") || emList[i].innerHTML.toLowerCase().includes("used")) {
                console.log('JAUS - The "Clear Field Configuration" Userscript was executed but detected the Field Configuration is IN USE. JAUS button not injected.');
                return true;
            }
        }
    }
    /* For Cloud */
    var sharedByClassList = document.getElementsByClassName("shared-by");
    if (sharedByClassList) {
        for (let i = 0; i < sharedByClassList.length; i++) {
            if (sharedByClassList[i].innerHTML.toLowerCase().includes("shared")) {
                console.log('JAUS - The "Clear Field Configuration" Userscript was executed but detected the Field Configuration is IN USE. JAUS button not injected.');
                return true;
            }
        }
    }
    /* For Server/DC */
    var usedByClassList = document.getElementsByClassName("used-by");
    if (usedByClassList) {
        for (let i = 0; i < usedByClassList.length; i++) {
            if (usedByClassList[i].innerHTML.toLowerCase().includes("used")) {
                console.log('JAUS - The "Clear Field Configuration" Userscript was executed but detected the Field Configuration is IN USE. JAUS button not injected.');
                return true;
            }
        }
    }
    return false;
}

/*
 * "MAIN" BODY:
 * Injects a button into the top menu.
 */
setTimeout(function () {
    let jausCreateButtonCloud = document.getElementById('createGlobalItem');
    let jausCreateButtonDC = document.getElementById('create_link');
    if (jausCreateButtonCloud && !jausIsConfigInUse()) {
        GM_addStyle(jausCSS);
        GM_addStyle(jausSpinnerCSS);
        let jausCreateButtonParent = jausCreateButtonCloud.parentNode;
        var jausNode = document.createElement("div");
        jausNode.id = 'jaus-cfc-main-menu';
        jausNode.className = jausCreateButtonParent.className;
        //aui-button aui-button-primary aui-style
        jausNode.innerHTML = '<div class="jaus jaus-main-button" id="jaus-cfc-main-button" accessKey="u" title="' + jausUserscriptVersion + '">Clear Field Config</div>';
        jausCreateButtonParent.appendChild(jausNode);
        let jausContextButton = document.getElementById("jaus-cfc-main-button");
        jausContextButton.addEventListener("click", toggleJAUSBox);
        window.JAUS = {};
        window.JAUS.isCloud = true;
        window.JAUS.links = {};
        window.JAUS.progress = 0;
    } else if (jausCreateButtonDC && !jausIsConfigInUse()) {
        GM_addStyle(jausCSS);
        GM_addStyle(jausSpinnerCSS);
        let jausCreateButtonParent = jausCreateButtonDC.parentNode.parentNode;
        var jausNode = document.createElement("li");
        jausNode.id = 'jaus-cfc-main-menu';
        //aui-button aui-button-primary aui-style
        jausNode.innerHTML = '<div class="jaus jaus-main-button" id="jaus-cfc-main-button" accessKey="u" title="' + jausUserscriptVersion + '">Clear Field Config</div>';
        jausCreateButtonParent.appendChild(jausNode);
        let jausContextButton = document.getElementById("jaus-cfc-main-button");
        jausContextButton.addEventListener("click", toggleJAUSBox);
        window.JAUS = {};
        window.JAUS.isCloud = false;
        window.JAUS.links = {};
        window.JAUS.progress = 0;
    }
}, 1000);
