// ==UserScript==
// @name        JAUS Bulk Option Insert
// @namespace   JAUS
// @version     1.0.0
// @author      Daiane Conte (https://www.linkedin.com/in/daianeconte) and Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez)
// @description Bulk inserts hardcoded options into the Select Custom Field.
// @homepage    https://jaus.atlassian.net/wiki/spaces/JAUS/pages/33224/Bulk+Option+Insert
// @supportURL  https://jaus.atlassian.net/servicedesk/customer/portals
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// @match       *://*/*/ViewCustomFields*
// @grant       GM_addStyle
// @downloadURL https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-bulk-option-insert.user.js
// @updateURL   https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-bulk-option-insert.user.js
// ==/UserScript==
let jausUserscriptVersion = 'v1.0.0'; // Always keep this in sync with the @version above

// Change this list to the desired values! (only works on custom field creation)
const jausListBulkOptionInsert = ["Option A", "Option B", "Option C", "Option D", "Option E", "Option F", "...", "Option Z"];

/**
 * Fills in the "custom-field-options-input" field and clicks the "custom-field-options-add" button for each value in [listBulkOptionInsert].
 */
function jausBulkOptionInsertOnFieldCreation() {
    if (document.getElementById('custom-field-options-input')) {
        for (let i = 0; i < jausListBulkOptionInsert.length; i++) {
            document.getElementById('custom-field-options-input').value = jausListBulkOptionInsert[i];
            document.getElementById('custom-field-options-add').click();
        }
    }
}

/*
 * "MAIN" BODY:
 * Injects a button into the header based on: https://codereview.stackexchange.com/questions/204768/tampermonkey-script-to-add-a-button-to-jira
 */
setTimeout(function() {
  let jausCreateButtonCloud = document.getElementById('createGlobalItem');
  let jausCreateButtonDC = document.getElementById('create_link');
  // For Cloud
  if(jausCreateButtonCloud) {
    let jausCreateButtonParent = jausCreateButtonCloud.parentNode.parentNode;
    let jausNode = document.createElement("div");
    jausNode.className = 'css-17l6d9b';
    jausNode.innerHTML = '<button id="jaus-context-button" data-hide-on-smallscreens="true" class="css-1tl77t8" type="button" tabindex="0" accesskey="u" title="' + jausUserscriptVersion + '"><span class="css-19r5em7">JAUS</span></button>';
    jausCreateButtonParent.appendChild(jausNode);
    let jausContextButton = document.getElementById("jaus-context-button");
    jausContextButton.addEventListener("click", jausBulkOptionInsertOnFieldCreation);
  }
  else if (jausCreateButtonDC) { // For Server/DC
    let jausCreateButtonParent = jausCreateButtonDC.parentNode.parentNode;
    let jausNode = document.createElement("li");
    jausNode.innerHTML = '<div class="aui-button aui-button-primary aui-style" id="jaus-context-button" accessKey="u" title="' + jausUserscriptVersion + '">JAUS</div>';
    jausCreateButtonParent.appendChild(jausNode);
    let jausContextButton = document.getElementById("jaus-context-button");
    jausContextButton.addEventListener("click", jausBulkOptionInsertOnFieldCreation);
  }
}, 1000);
