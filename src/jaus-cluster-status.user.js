// ==UserScript==
// @name        JAUS Cluster Status
// @namespace   JAUS
// @version     1.0.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez)
// @description Checks the /cluster/nodes and index/summary on all cluster nodes and reports.
// @homepage    https://jaus.atlassian.net/wiki/spaces/JAUS/pages/328682/Cluster+Status
// @supportURL  https://jaus.atlassian.net/servicedesk/customer/portals
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// @match       *://*/*/cluster-monitoring*
// @grant       GM_xmlhttpRequest
// @grant       GM_addStyle
// @downloadURL https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-cluster-status.user.js
// @updateURL   https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-cluster-status.user.js
// ==/UserScript==
let jausUserscriptVersion = 'v1.0.0'; // Always keep this in sync with the @version above

// Spinner CSS from w3schools.com/howto/howto_css_loader.asp
var jausSpinnerCSS = `
              .jausSpinnerClass {
                 border: 4px solid #DFE1E6;
                 border-top: 4px solid #0052CC;
                 border-radius: 50%;
                 width: 24px;
                 height: 24px;
                 animation: spin 2s linear infinite;
                 text-align: center;
               }
               @keyframes spin {
                 0% { transform: rotate(0deg); }
                 100% { transform: rotate(360deg); }
               }`;

function printVersion() {
  return '<a href="https://jaus.atlassian.net/wiki/spaces/JAUS/pages/328682/Cluster+Status" target="_blank">' + jausUserscriptVersion + '&nearr;</a>';
}

/**
 * Injects or removes the background "blanket", blurring everything else.
 * If the blanket is present, removes it — if it's not present, injects it.
 */
function jausToggleBlanket() {
  let jausBlanket = document.getElementById('jaus-blanket');
  if (jausBlanket) {
    jausBlanket.parentNode.removeChild(jausBlanket);
  }
  else {
    let blanket = document.createElement("div");
    blanket.id = "jaus-blanket";
    blanket.className = "aui-blanket";
    blanket.tabIndex = "0";
    blanket.ariaHidden = "false";
    document.body.appendChild(blanket);
  }
}

function jausInjectBox() {
  jausToggleBlanket();
  let newDiv = document.createElement("div");
  newDiv.innerHTML = '' +
    '<div id="jaus-cluster-status-box" class="aui-dialog2 aui-layer jira-dialog2 jira-dialog-core aui-dialog2-large jira-dialog-open jira-dialog-content-ready" role="dialog" aria-labelledby="jira-dialog2__heading" style="z-index: 3000;" data-aui-focus="false" data-aui-blanketed="true" open="" tabindex="-1">' +
    '  <header class="aui-dialog2-header jira-dialog-core-heading">' +
    '    <h2 id="jira-dialog2__heading" title="JAUS Cluster Status">JAUS Cluster Status</h2>' +
    '    <div class="aui-toolbar2 qf-form-operations" role="toolbar"><div class="aui-toolbar2-inner"><div class="aui-toolbar2-secondary">' + printVersion() + '</div></div></div>' +
    '  </header>' +
    '  <div class="aui-dialog2-content jira-dialog-core-content" id="jaus-cluster-status-content">' +
    '    <center><div class="jausSpinnerClass" id="jausSpinner" valign="middle"></center>' +
    '  </div>' +
    '  <footer class="aui-dialog2-footer"><div class="buttons-container form-footer"><div class="buttons" align="right"><button type="button" class="aui-button aui-button-link cancel" id="jaus-close-box-link">Close</button></div></div></footer>' +
    '</div>';
  document.body.appendChild(newDiv);
  let closeBoxLink = document.getElementById('jaus-close-box-link');
  closeBoxLink.addEventListener("click", jausCloseBox);
  jausFetchClusterInfo();
}

// Copied from https://www.w3schools.com/howto/howto_js_copy_clipboard.asp
function jausCopyToClipboard() {
  var copyText = document.getElementById("jaus-cluster-status-content-textarea");
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */
  navigator.clipboard.writeText(copyText.value);
}

function jausCloseBox() {
  window.JAUS.fetchedNodeCount = 0;
  let jausBox = document.getElementById('jaus-cluster-status-box');
  if(jausBox){
    jausBox.parentNode.removeChild(jausBox);
  }
  jausToggleBlanket();
}

/*
 * Spinner from https://aui.atlassian.com/aui/7.9/docs/spinner.html doesn't work on Cloud (as of Oct 2021)
 * So using spinner from CSS from w3schools.com/howto/howto_css_loader.asp
 */
function jausRemoveSpinner() {
  let jausSpinner = document.getElementById("jausSpinner");
  if (jausSpinner) {
    jausSpinner.parentNode.removeChild(jausSpinner);
  }
}

/**
 * Fetches the /cluster/nodes info and the /index/summary of each node.
 * Injects a <textarea> inside the "jaus-cluster-status-content" div and appends the nodes info into the textarea value.
 */
function jausFetchClusterInfo() {
  GM_xmlhttpRequest({
    method:'GET',
    url: window.JAUS.restAPIURL + 'cluster/nodes',
    responseType: 'json',
    onload: function(nodesResponse) {
      //console.log(JSON.stringify(nodesResponse.response, null, 2));
      for (let i=0; i < nodesResponse.response.length; i++) {
        jausFetchNodeIndexSummary(nodesResponse.response[i]);
      }
      setTimeout(function() {
        do {
          if (window.JAUS.fetchedNodeCount == nodesResponse.response.length) {
            let jausContent = document.getElementById('jaus-cluster-status-content');
            if (jausContent) {
              jausRemoveSpinner();
              jausContent.innerHTML = '<textarea id="jaus-cluster-status-content-textarea" rows="256" cols="100%"></textarea>'
              for (let i=0; i<nodesResponse.response.length; i++) {
                jausInjectNodeInfo(nodesResponse.response[i], document.getElementById('jaus-cluster-status-content-textarea'));
              }
              console.log(JSON.stringify(nodesResponse.response, null, 2));
            }
          }
          else {
            console.log("JAUS waiting...");
          }
        } while (window.JAUS.fetchedNodeCount < nodesResponse.response.length); 
      }, 1000);
    }
  });
}

/**
 * 
 * @param {json} nodeJSON - the JSON representation of the node.
 * @param {element} textArea - the HTML textarea element into which value this functions appends the node info. 
 */
function jausInjectNodeInfo(nodeJSON, textArea) {
  textArea.value += JSON.stringify(nodeJSON, null, 2) + '\n\n\n';
 
}

/**
 * Fetches the /rest/api/2/index/summary of a node.
 * @param {json} nodeJSON - The node to report the Index Summary.
 */
function jausFetchNodeIndexSummary(nodeJSON) {
  GM_xmlhttpRequest({
    method:'GET',
    url: window.JAUS.restAPIURL + 'index/summary',
    responseType: 'json',
    onload: function(indexResponse) {
      nodeJSON.indexSummary = indexResponse.response;
      window.JAUS.fetchedNodeCount++;
    }
  });
}

/*
 * "MAIN" BODY:
 * Injects a button into the header based on: https://codereview.stackexchange.com/questions/204768/tampermonkey-script-to-add-a-button-to-jira
 */
setTimeout(function() {
  let jausCreateButtonDC = document.getElementById('create_link');
  if (jausCreateButtonDC) { // For Server/DC
    GM_addStyle(jausSpinnerCSS);
    window.JAUS = {};
    window.JAUS.fetchedNodeCount = 0;
    window.JAUS.restAPIURL = window.location.href.split(/plugins\/servlet\/cluster-monitoring/).shift() + 'rest/api/2/';
    GM_addStyle(jausSpinnerCSS);
    let jausCreateButtonParent = jausCreateButtonDC.parentNode.parentNode;
    let jausNode = document.createElement("li");
    jausNode.innerHTML = '<div class="aui-button aui-button-primary aui-style" id="jaus-context-button" accessKey="u" title="' + jausUserscriptVersion + '">JAUS</div>';
    jausCreateButtonParent.appendChild(jausNode);
    let jausContextButton = document.getElementById("jaus-context-button");
    jausContextButton.addEventListener("click", jausInjectBox);
  }
}, 1000);
