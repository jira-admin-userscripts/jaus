// ==UserScript==
// @name        JAUS 2.0 Draft
// @namespace   JAUS
// @version     0.0.1
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez)
// @description JAUS 2.0 Draft - Work in Progress
// @homepage    
// @supportURL  
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// @match       http://192.168.0.106:8081/*
// @match       https://jaus.atlassian.net/jira/*
// @grant       none
// @grant       GM_addStyle
// @downloadURL 
// @updateURL   
// ==/UserScript==
let jausUserscriptVersion = 'v0.0.1'; // Always keep this in sync with the @version above

// Control Flag to check/uncheck the row of fields
let jausControlFlag = false;

// Spinner CSS from w3schools.com/howto/howto_css_loader.asp
var jausCSS = `
.jausContextButton {
  color: #555555;
  font-family: "Segoe UI", Helvetica, sans-serif, monospace;
  font-weight: bold;
  text-align: center;
  margin-left: 30px;
  border-style: solid;
  border-width: 1px;
  border-radius: 5px;
  border-color: GoldenRod;
  background-color: gold;
  padding: 4px 10px;
  cursor: pointer;
  transition-duration: 0.1s;
  transition-property: background-color;
  transition-timing-function: ease-out;
}

.jausContextButton:hover {
  background-color: #ffe033;
}

.jausBlanket {
  background-color: rgba(23,43,77,0.45);
  height: 100%;
  width: 100%;
  position: fixed;
  z-index: 2500;
  left: 0px;
  top: 0px;
}

.jausBox {
  top: 100px; right: 0; bottom: 100px; left: 0;
  margin: auto;
  position: fixed;
  display: block;
  background-color: white;
  z-index: 3000;
  width: 35%;
  height: 50%;
  border-style: solid;
  border-color: white;
  border-radius: 5px;
  color: black;
  font-family: "Segoe UI", Helvetica, sans-serif, monospace;
}

.jausBox header {
  position: sticky;
  height: 60px;
  display: flex;
  /*border-style: solid; border-color: red; border-width: 1px;*/
  border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: #ebecf0;
}

.jausBox header h1 {
  color: red;
  /*border-style: solid; border-color: goldenrod; border-width: 1px;*/
  margin-left: 0px;
  tetx-align: left;
  position: sticky;
  padding: 5px;
}

.jausBox header nav {
  position: absolute;
  /*border-style: solid; border-color: blue; border-width: 1px;*/
  margin-left: auto;
  padding: 5px;
  right: 0px;
}

.jausBox article {
  padding-left: 10px;
  color: gold;
  display: block;
  position: absolute;
  /*border-style: solid; border-color: red; border-width: 1px;*/
  top: 62px; right: 0px; bottom: 52px; left: 0px;
  overflow: auto;
}

.jausBox footer {
  color: #555555;
  position: absolute;
  display: flex;
  /*border-style: solid; border-color: red; border-width: 1px;*/
  bottom: 0px;
  right: 0px;
  left: 0px;
  height: 50px;
  border-top-style: solid; border-top-width: 2px; border-top-color: #ebecf0;
}

.jausBox footer p {
  position: absolute;
  right: 0px;
  bottom: 0px;
  padding: 5px;
  font-style: italic;
  font-family: monospace, sans-serif, v, "Segoe UI";
}
`;

/**
 * Injects or removes the background "blanket", blurring everything else.
 * If the blanket is present, removes it — if it's not present, injects it.
 */
function jausToggleBlanket() {
  let jausBlanket = document.getElementById('jaus-blanket');
  if (jausBlanket) {
    jausBlanket.parentNode.removeChild(jausBlanket);
  }
  else {
    let blanket = document.createElement("div");
    blanket.id = "jaus-blanket";
    blanket.className = "jausBlanket";
    document.body.appendChild(blanket);
    blanket.addEventListener("click", jausToggleBlanket);
  }
}

function toggleJAUSBox() {
  let jausBox = document.getElementById('jaus-box');
  let jausBlanket = document.getElementById('jaus-blanket');
  let boxRemoved = false;
  if (jausBox) {
    jausBox.parentNode.removeChild(jausBox);
    boxRemoved = true;
  }
  if (jausBlanket) {
    jausBlanket.parentNode.removeChild(jausBlanket);
    boxRemoved = true;
  }
  if (!boxRemoved) {
    let jausBlanket = document.createElement("div");
    jausBlanket.id = "jaus-blanket";
    jausBlanket.className = "jausBlanket";
    document.body.appendChild(jausBlanket);
    jausBlanket.addEventListener("click", toggleJAUSBox);
    
    jausBox = document.createElement("section");
    jausBox.id = 'jaus-box';
    jausBox.className = 'jausBox';
    jausBox.innerHTML = '<header id="jaus-box-header"><h1>JAUS 2.0</h1><nav id="jaus-nav"><a href="#" id="jaus-close-button">Close</a></nav></header><article id="jaus-content"><p>Content</p><p>Content</p><p>Content</p><p>Content</p><p>Content</p><p>Content</p><p>Content</p><p>Content</p><p>Content</p><p>Content</p></article><footer id="jaus-box-footer"><p>' + jausUserscriptVersion + '</p></footer>';
    document.body.appendChild(jausBox);
    let jausCloseButton = document.getElementById('jaus-close-button');
    jausCloseButton.addEventListener("click", toggleJAUSBox);
  }
}

function jausShowReorderStatusBox() {
  jausToggleBlanket();
  const newDiv = document.createElement("div");
  newDiv.innerHTML = '' +
    '<div id="jaus-reorder-status-box" class="jira-dialog box-shadow jira-dialog-open popup-width-medium jira-dialog-content-ready" style="width: 540px; margin-left: -271px; margin-top: -198.5px;">' +
    '  <div class="jira-dialog-heading"><h2 title="Edit status">JAUS Reorder Status</h2>' +
    '    <div class="aui-toolbar2 qf-form-operations" role="toolbar"><div class="aui-toolbar2-inner"><div class="aui-toolbar2-secondary">' + printVersion() + '</div></div></div></div>' + 
    '  <div class="jira-dialog-content">' +
    '    <div class="dialog-title hidden"></div>' +
    '    <form class="aui" action="javascript:void(0);" method="get" name="jaus-reorder-status-form">' +
    '      <div class="form-body" style="max-height: 267px;">' +
    '        <div class="field-group"><label for="name">Status Id</label><input class="text" type="text" name="jaus_status_id" id="jaus_status_id" value="" placeholder="Status id"/></div>' +
    '        <div class="field-group"><label for="name">Status Direction</label><select class="select" id="jaus_status_direction" type="text" placeholder="bwa?"><option value="up">Up</option><option value="down">Down</option></select></div>' +
    '        <div class="field-group"><label for="name">Status Position Shift</label><input class="text" type="text" name="jaus_status_shift" id="jaus_status_shift" value="" placeholder="Number of positions to move"/></div>' +
    '      </div>' +
    '      <div class="form-footer"><div class="buttons-container"><div class="buttons" id="jaus-buttons"><span class="throbber"></span><input type="submit" id="jaus_reorder_status_submit" class="aui-button" name="Reorder" value="Reorder" resolved=""><a href="#" class="cancel" id="jaus-close-box-link" accesskey="`">Cancel</a></div></div></div>' +
    '    </form>' +
    '  </div>' +
    '</div>';
  document.body.appendChild(newDiv);
  let jausSubmit = document.getElementById('jaus_reorder_status_submit');
  jausSubmit.addEventListener("click", jausReorderStatus);
  let closeBoxLink = document.getElementById('jaus-close-box-link');
  closeBoxLink.addEventListener("click", jausCloseBox);
}

function jausCloseBox() {
  let jausBox = document.getElementById('jaus-reorder-status-box');
  if(jausBox){
    jausBox.parentNode.removeChild(jausBox);
  }
  jausToggleBlanket();
}

/**
 * Checks/unchecks all boxes in the Custom Field Screen association screen.
 */
function adminCheckAllFieldScreens() {
  let boxes = document.getElementsByName('associatedScreens');
  if (boxes) {
    for (let i = 0; i < boxes.length; i++) {
      if (jausControlFlag)
        boxes[i].checked = ''
      else
        boxes[i].checked = 'checked';
    }
    jausControlFlag = !jausControlFlag;
  }
}

/*
 * Checks/unchecks the checboxes in the current screen.
 */
function jausAdminCheckAll() {
  adminCheckAllFieldScreens();
  // Add more functions that work for other screens/checkboxes
}

/*
 * "MAIN" BODY:
 * Injects a button into the header based on: https://codereview.stackexchange.com/questions/204768/tampermonkey-script-to-add-a-button-to-jira
 */
//setTimeout(function() {
  let jausCreateButtonCloud = document.getElementById('createGlobalItem');
  let jausCreateButtonDC = document.getElementById('create_link');
  if(jausCreateButtonCloud) {
    GM_addStyle(jausCSS);
    let jausCreateButtonParent = jausCreateButtonCloud.parentNode;
    var jausNode = document.createElement("div");
    jausNode.id = 'jaus-menu';
    jausNode.className = jausCreateButtonParent.className;
    //aui-button aui-button-primary aui-style
    jausNode.innerHTML = '<div class="jausContextButton" id="jaus-context-button" accessKey="u" title="' + jausUserscriptVersion + '">JAUS</a>';
    jausCreateButtonParent.appendChild(jausNode);
    let jausContextButton = document.getElementById("jaus-context-button");
    jausContextButton.addEventListener("click", toggleJAUSBox);
  }
  else if (jausCreateButtonDC) {
    GM_addStyle(jausCSS);
    let jausCreateButtonParent = jausCreateButtonDC.parentNode.parentNode;
    var jausNode = document.createElement("li");
    jausNode.id = 'jaus-menu';
    //aui-button aui-button-primary aui-style
    jausNode.innerHTML = '<div class="jausContextButton" id="jaus-context-button" accessKey="u" title="' + jausUserscriptVersion + '">JAUS</a>';
    jausCreateButtonParent.appendChild(jausNode);
    let jausContextButton = document.getElementById("jaus-context-button");
    jausContextButton.addEventListener("click", toggleJAUSBox);
  }
//}, 1000);