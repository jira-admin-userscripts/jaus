// ==UserScript==
// @name        JAUS Confluence Set Read Only Space Permissions
// @namespace   JAUS
// @version     1.0.0
// @author      Rafael Corredor (https://www.linkedin.com/in/rafaelcorredor) and Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez)
// @description Clears all permission checkboxes to make the space Read Only.
// @homepage    https://jaus.atlassian.net/wiki/spaces/JAUS/pages/1146884/Confluence+Set+Read+Only+Space+Permissions
// @supportURL  https://jaus.atlassian.net/servicedesk/customer/portals
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// @match       https://*.atlassian.net/wiki/admin/permissions/editdefaultspacepermissions.action*
// @match       https://*.atlassian.net/wiki/spaces/editspacepermissions.action*
// @match       *://*/*/editdefaultspacepermissions.action*
// @match       *://*/*/editspacepermissions.action*
// @grant       none
// @downloadURL https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-confluence-set-read-only-permissions.user.js
// @updateURL   https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-confluence-set-read-only-permissions.user.js
// ==/UserScript==
let jausUserscriptVersion = 'v1.0.0'; // Always keep this in sync with the @version above

/**
 * Checks/unchecks all boxes it finds in the screen based on it's name.
 */
function jausSetReadOnlyCheckboxes() {
  let boxes = document.querySelectorAll('input[type=checkbox]');
  if (boxes) {
    for (let i = 0; i < boxes.length; i++) {
      if (boxes[i].name.startsWith('confluence_checkbox_viewspace_anonymous'))
        continue;
      if (boxes[i].name.startsWith('confluence_checkbox_viewspace_'))
        boxes[i].checked = 'checked'
      else
        boxes[i].checked = '';
    }
  }
}

/*
 * "MAIN" BODY:
 * Injects a button into the header based on: https://codereview.stackexchange.com/questions/204768/tampermonkey-script-to-add-a-button-to-jira
 */
setTimeout(function() {
  let jausCreateButtonCloud = document.getElementById('createGlobalItem');
  let jausCreateButtonDC = document.getElementById('quick-create-page-button');
  if(jausCreateButtonCloud) {
    //GM_addStyle(jausSpinnerCSS);
    let jausCreateButtonParent = jausCreateButtonCloud.parentNode.parentNode.parentNode.parentNode;
    //console.debug(jausCreateButtonParent.className);
    let jausNode = document.createElement("div");
    jausNode.className = jausCreateButtonCloud.parentNode.parentNode.parentNode.className;
    jausNode.innerHTML = '<div role="presentation"><div class="css-17l6d9b"><button id="jaus-context-button" data-hide-on-smallscreens="true" class="css-1lggz63" type="button" tabindex="0" accesskey="u" title="' + jausUserscriptVersion + '"><span class="css-19r5em7">JAUS</span></button></div></div>';
    jausCreateButtonParent.appendChild(jausNode);
    let jausContextButton = document.getElementById("jaus-context-button");
    jausContextButton.addEventListener("click", jausSetReadOnlyCheckboxes);
  }
  else if (jausCreateButtonDC) {
    //GM_addStyle(jausSpinnerCSS);
    let jausCreateButtonParent = jausCreateButtonDC.parentNode.parentNode;
    let jausNode = document.createElement("li");
    jausNode.innerHTML = '<div class="aui-button aui-button-primary aui-style" id="jaus-context-button" accessKey="u" title="' + jausUserscriptVersion + '">JAUS</div>';
    jausCreateButtonParent.appendChild(jausNode);
    let jausContextButton = document.getElementById("jaus-context-button");
    jausContextButton.addEventListener("click", jausSetReadOnlyCheckboxes);
  }
}, 1000);