// ==UserScript==
// @name        JAUS Bulk Edit Request Participant
// @namespace   JAUS
// @version     1.1.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez)
// @description Injects a screen to bulk add or remove a participant to issues from a search result.
// @homepage    https://jaus.atlassian.net/wiki/spaces/JAUS/pages/1146943/Bulk+Edit+Request+Participant
// @supportURL  https://jaus.atlassian.net/servicedesk/customer/portals
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// @match       *://*/issues/*
// @match       *://*/*/issues/*
// @grant       none
// @grant       GM_addStyle
// @grant       GM_xmlhttpRequest
// @downloadURL https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-bulk-edit-request-participant.user.js
// @updateURL   https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-bulk-edit-request-participant.user.js
// ==/UserScript==
let jausUserscriptVersion = 'v1.1.0'; // Always keep this in sync with the @version above

/*
 * Spinner from https://aui.atlassian.com/aui/7.9/docs/spinner.html
 * So using spinner from CSS from w3schools.com/howto/howto_css_loader.asp
 */
var jausSpinnerCSS = `
.jausSpinnerClass {
    visibility: hidden;
    margin-left: auto;
    margin-right: auto;
    margin-top: 0px;
    position: center;
    border: 4px solid #DFE1E6;
    border-top: 4px solid #0052CC;
    border-radius: 50%;
    width: 24px;
    height: 24px;
    animation: spin 2s linear infinite;
    text-align: center;
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}`;

var jausCSS = `
.jaus {
    all: none;
    font-size: 14px;
    font-family: "Segoe UI", Helvetica, sans-serif, monospace;
}

.jaus-main-button {
    color: #555555;
    font-weight: bold;
    text-align: center;
    margin-left: 30px;
    border-style: solid;
    border-width: 1px;
    border-radius: 5px;
    border-color: GoldenRod;
    background-color: gold;
    padding: 4px 10px;
    cursor: pointer;
    transition-duration: 0.1s;
    transition-property: background-color;
    transition-timing-function: ease-out;
}

.jaus-main-button:hover {
    background-color: #ffe033;
}

.jaus-blanket {
    background-color: rgba(23, 43, 77, 0.45);
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 2500;
    left: 0px;
    top: 0px;
}

.jaus-box {
    top: 100px;
    right: 0;
    bottom: 100px;
    left: 0;
    margin: auto;
    position: fixed;
    display: block;
    background-color: white;
    z-index: 3000;
    width: 35%;
    height: 370px;
    max-height: 50%;
    min-height: 300px;
    border-style: solid;
    border-color: white;
    border-radius: 5px;
    font-family: "Segoe UI", Helvetica, sans-serif, monospace;
}

.jaus-box header {
    position: sticky;
    height: 60px;
    display: flex;
    border-bottom-style: solid;
    border-bottom-width: 2px;
    border-bottom-color: #ebecf0;
}

.jaus-box header h1 {
    margin-left: 0px;
    text-align: left;
    font-size: 20px;
    position: sticky;
    color: rgb(23, 43, 77);
    padding: 10px 10px 10px 20px;
}

.jaus-box header nav {
    position: absolute;
    margin-left: auto;
    padding: 5px;
    right: 0px;
}

.jaus-box article {
    display: block;
    position: absolute;
    top: 62px;
    right: 0px;
    bottom: 52px;
    left: 0px;
    overflow: auto;
    padding: 30px;
}

.jaus-box article p {
    color: rgb(107, 119, 140);
    font-size: 14px;
    line-height: 20px;
    display: inline-block;
}

.jaus-box article label {
    font-weight: 500;
    width: 130px;
    text-align: right;
    display: inline-grid;
    padding: 3px 20px 3px 4px;
}

.jaus-berp-field-feedback {
    font-weight: 500;
    width: 100px;
    height: 20px;
    display: inline;
    visibility: visible;
    padding: 3px 5px 3px 5px;
    margin-left: 5px;
    position: absolute;
    vertical-align: middle;
}

#jaus-berp-rp-found {
    margin-left: 0px;
    position: absolute;
    visibility: hidden;
    vertical-align: middle;
    color: rgb(0, 135, 90);
}

#jaus-berp-rp-not-found {
    margin-left: 0px;
    position: absolute;
    visibility: hidden;
    vertical-align: middle;
    color: rgb(255, 171, 0);
}

#jaus-berp-search-button {
    margin-left: 10px;
}

.jaus-box footer {
    color: #555555;
    position: absolute;
    display: flex;
    bottom: 0px;
    right: 0px;
    left: 0px;
    height: 50px;
    border-top-style: solid;
    border-top-width: 2px;
    border-top-color: #ebecf0;
}

.jaus-box footer p {
    position: absolute;
    right: 0px;
    bottom: 0px;
    padding: 5px;
    font-style: italic;
    font-family: monospace, sans-serif, v, "Segoe UI";
    display: inline-grid;
}

.jaus-text-field {
    background-color: rgb(250, 251, 252);
    border-style: solid;
    border-width: 2px;
    border-color: rgb(223, 225, 230);
    font-size: 14px;
    padding: 3px 4px;
    width: 300px;
    line-height: 20px;
    border-radius: 3px;
    color: rgb(23, 43, 77);
    font-weight: 400;
    display: inline-grid;
}

.jaus-select-field {
    background-color: rgb(235, 236, 240);
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill='%23344563' d='M6.744 8.744a1.053 1.053 0 000 1.49l4.547 4.557a1 1 0 001.416 0l4.55-4.558a1.051 1.051 0 10-1.488-1.488l-3.77 3.776-3.768-3.776a1.051 1.051 0 00-1.487 0z'/%3E%3C/svg%3E");
    background-position: 100% 50%;
    background-repeat: no-repeat;
    border-style: solid;
    border-width: 2px;
    border-color: rgb(235, 236, 240);
    display: inline-grid;
    appearance: none;
    border-radius: 3px;
    font-weight: 400;
    font-size: 14px;
    color: rgb(23, 43, 77);
    height: 30px;
    padding: 3px 0px 3px 5px;
    width: 312px;
}

.jaus-berp-lower-section {
    border-top-style: solid;
    border-top-width: 2px;
    border-top-color: #ebecf0;
    margin-top: 15px;
    width: 100%;
    position: relative;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
}

.jaus-berp-search-feedback {
    font-weight: 500;
    width: 100%;
    height: 15px;
    padding: 3px 5px 3px 5px;
    position: relative;
    vertical-align: middle;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    margin-top: 10px;
}

#jaus-berp-commit-button {
    visibility: hidden;
    margin-top: 10px;
    margin-left: 5px;
    padding: 3px 5px 3px 5px;
}
`;

function jausPrintVersion() {
    return '<a href="https://jaus.atlassian.net/wiki/spaces/JAUS/pages/1146943/Bulk+Edit+Request+Participant" target="_blank">' + jausUserscriptVersion + '&nearr;</a>';
}

/**
 * Injects or removes the background "blanket", blurring everything else.
 * If the blanket is present, removes it — if it's not present, injects it.
 */
function jausToggleBlanket() {
    let jausBlanket = document.getElementById('jaus-berp-blanket');
    if (jausBlanket) {
        jausBlanket.parentNode.removeChild(jausBlanket);
    } else {
        let blanket = document.createElement("div");
        blanket.id = "jaus-berp-blanket";
        blanket.className = "jaus jaus-blanket";
        document.body.appendChild(blanket);
        blanket.addEventListener("click", jausToggleBlanket);
    }
}

function jausValidateRequestParticipant() {
    let requestURL = window.JAUS.restAPIJira + 'user?username=' + document.getElementById('jaus-berp-request-participant').value;
    if (window.JAUS.isCloud) {
        requestURL = window.JAUS.restAPIJira + 'user?accountId=' + document.getElementById('jaus-berp-request-participant').value;
    }
    console.log(requestURL);
    GM_xmlhttpRequest({
        method: 'GET',
        headers: {"Content-Type": "application/json;charset=UTF-8"},
        url: requestURL,
        responseType: 'json',
        onload: function (responseRP) {
            console.log(responseRP.status);
            let success = document.getElementById('jaus-berp-rp-found');
            let failure = document.getElementById('jaus-berp-rp-not-found');
            if (responseRP.status == 200) {
                window.JAUS.userFound = true;
                document.getElementById('jaus-berp-search-button').disabled = false;
                success.style.visibility = "visible";
                failure.style.visibility = "hidden";
            } else {
                window.JAUS.userFound = false;
                document.getElementById('jaus-berp-search-button').disabled = true;
                success.style.visibility = "hidden";
                failure.style.visibility = "visible";
            }
        }
    });
}

function jausRunJQL() {
    let jql = document.getElementById("jaus-berp-search").value;
    if (!jql) {
        jql = 'created > now()'; // Prevents "full search"
    }
    if (!window.JAUS.userFound) {
        document.getElementById('jaus-berp-search-feedback-message').innerHTML = 'Please provide the username (for DC) or accountId (for Cloud).';
        window.JAUS.issues = {};
        document.getElementById('jaus-berp-commit-button').style.visibility = "hidden";
        return false;
    }
    GM_xmlhttpRequest({
        method: 'GET',
        headers: {"Content-Type": "application/json;charset=UTF-8"},
        url: window.JAUS.restAPIJira + 'search?jql=' + jql + '&fields=key',
        responseType: 'json',
        onload: function (responseJQL) {
            let total = 0;
            if (responseJQL.response.total) {
                total = responseJQL.response.total;
            }
            if (total > 50) {
                document.getElementById('jaus-berp-search-feedback-message').innerHTML = '' + total + ' issues matched (only 50 are updated at each execution).';
                document.getElementById('jaus-berp-commit-button').style.visibility = "visible";
            } else if (total == 1) {
                document.getElementById('jaus-berp-search-feedback-message').innerHTML = 'Only ' + total + ' issue matched.';
                document.getElementById('jaus-berp-commit-button').style.visibility = "visible";
            } else if (total > 1) {
                document.getElementById('jaus-berp-search-feedback-message').innerHTML = '' + total + ' issues matched.';
                document.getElementById('jaus-berp-commit-button').style.visibility = "visible";
            } else {
                document.getElementById('jaus-berp-commit-button').style.visibility = "hidden";
            }
            window.JAUS.issues = responseJQL.response.issues;
        }
    });
    return true;
}

function jausUpdateIssues() {
    if (window.JAUS.issues) {
        let httpMethod = "POST";
        if (document.getElementById('jaus-berp-action').value == "remove") {
            httpMethod = "DELETE";
        }
        let requestData = '{"usernames":["' + document.getElementById("jaus-berp-request-participant").value + '"]}'
        if (window.JAUS.isCloud) {
            requestData = '{"accountIds":["' + document.getElementById("jaus-berp-request-participant").value + '"]}'
        }
        /* Commented lines below have the logic to log the updated issues — but the log's lost once the page's refreshed.*/
        //let issueKeys = [];
        document.getElementById('jaus-berp-commit-button').style.visibility = "hidden";
        document.getElementById('jaus-berp-search-feedback-message').style.visibility = "hidden";
        document.getElementById('jaus-berp-spinner').style.visibility = "visible";
        //let httpStatus = 0;
        let requestURL = "";
        for (let i = 0; i < window.JAUS.issues.length; i++) {
            requestURL = window.JAUS.restAPIJiraSD + window.JAUS.issues[i].key + '/participant';
            httpStatus = jausUpdateIssueRequest(httpMethod, requestURL, requestData);
            /*if (httpStatus == 200) {
                issueKeys.push(window.JAUS.issues[i].key);
            }*/
        }
    }
    setTimeout(function () {
        /*if (issueKeys) {
            console.info("JAUS - issues updated: " + issueKeys);
        }*/
        document.getElementById('jaus-berp-form-search').reset();
        document.getElementById('jaus-berp-form-commit').submit();
    }, 5000);
}

function jausUpdateIssueRequest(httpMethod, requestURL, requestData) {
    GM_xmlhttpRequest({
        method: httpMethod,
        headers: {"Content-Type": "application/json"},
        url: requestURL,
        data: requestData,
        responseType: 'json',
        onload: function (responseUpdate) {
            console.debug("JAUS - jausUpdateIssueRequest received HTTP status: " + responseUpdate.status);
            return responseUpdate.status;
        }
    });
}

function toggleJAUSBox() {
    let jausBox = document.getElementById('jaus-berp-box');
    let jausBlanket = document.getElementById('jaus-berp-blanket');
    let boxRemoved = false;
    if (jausBox) {
        jausBox.parentNode.removeChild(jausBox);
        boxRemoved = true;
    }
    if (jausBlanket) {
        jausBlanket.parentNode.removeChild(jausBlanket);
        boxRemoved = true;
    }
    if (!boxRemoved) {
        let jausBlanket = document.createElement("div");
        jausBlanket.id = "jaus-berp-blanket";
        jausBlanket.className = "jaus jaus-blanket";
        document.body.appendChild(jausBlanket);
        jausBlanket.addEventListener("click", toggleJAUSBox);

        jausBox = document.createElement("section");
        jausBox.id = 'jaus-berp-box';
        jausBox.className = 'jaus jaus-box';
        let advancedSearchBox = document.getElementById("advanced-search"); // Jira's field
        let currentSearch = '';
        if (advancedSearchBox) {
            currentSearch = advancedSearchBox.value;
        }
        jausBox.innerHTML = '<header id="jaus-berp-box-header"><h1>JAUS - Bulk Edit Request Participant</h1><nav id="jaus-berp-nav"><a href="#" id="jaus-berp-close-button">Close</a></nav></header><article id="jaus-berp-content"><section>' +
            '<form id="jaus-berp-form-search" method="POST" action="">' +
            '<p><label for="jaus-berp-request-participant">Request Participant </label><input class="jaus jaus-text-field" type="text" name="jaus-berp-request-participant" id="jaus-berp-request-participant" value="" placeholder="Request Participant" required></input><aside id="jaus-berp-request-participant-feedback" class="jaus jaus-berp-field-feedback">' +
            '<span id="jaus-berp-rp-found" class="jaus aui-icon aui-icon-small aui-iconfont-success"></span>' +
            '<span id="jaus-berp-rp-not-found" class="jaus aui-icon aui-icon-small aui-iconfont-warning"></span>' +
            '</aside></p>' +
            '<p><label for="jaus-berp-action">Action to perform </label><select class="jaus jaus-select-field" id="jaus-berp-action" required><option value="add" selected>Add to issues</option><option value="remove">Remove from issues</option></select></p>' +
            '<p><label for="jaus-berp-search">Issues to update </label><input class="jaus jaus-text-field" type="text" name="jaus-berp-search" id="jaus-berp-search" value="' + currentSearch + '" placeholder="Type the JQL here" required></input>' +
            '<button type="button" id="jaus-berp-search-button" disabled>Run JQL</button></p>' +
            '</form></section>' +
            '<section id="jaus-berp-lower-section" class="jaus jaus-berp-lower-section">' +
            '<aside id="jaus-berp-search-feedback" class="jaus jaus-berp-search-feedback"><span id="jaus-berp-search-feedback-message"></span>' +
            '<div class="jaus jausSpinnerClass" id="jaus-berp-spinner"></div>' +
            '</aside>' +
            '<form id="jaus-berp-form-commit" method="POST" action=""><button type="button" id="jaus-berp-commit-button">Update issues!</button></form>' +
            '</section>' +
            '</article><footer id="jaus-box-footer"><p>' + jausPrintVersion() + '</p></footer>';
        document.body.appendChild(jausBox);
        document.getElementById('jaus-berp-close-button').addEventListener("click", toggleJAUSBox);
        document.getElementById('jaus-berp-search-button').addEventListener("click", jausRunJQL);
        document.getElementById('jaus-berp-commit-button').addEventListener("click", jausUpdateIssues);
        document.getElementById('jaus-berp-request-participant').addEventListener("blur", jausValidateRequestParticipant);
    }
}

/*
 * "MAIN" BODY:
 * Injects a button into the header based on: https://codereview.stackexchange.com/questions/204768/tampermonkey-script-to-add-a-button-to-jira
 */
setTimeout(function () {
    let jausCreateButtonCloud = document.getElementById('createGlobalItem');
    let jausCreateButtonDC = document.getElementById('create_link');
    if (jausCreateButtonCloud) {
        GM_addStyle(jausCSS);
        GM_addStyle(jausSpinnerCSS);
        let jausCreateButtonParent = jausCreateButtonCloud.parentNode;
        var jausNode = document.createElement("div");
        jausNode.id = 'jaus-berp-main-menu';
        jausNode.className = jausCreateButtonParent.className;
        //aui-button aui-button-primary aui-style
        jausNode.innerHTML = '<div class="jaus jaus-main-button" id="jaus-berp-main-button" accessKey="u" title="' + jausUserscriptVersion + '">Bulk Edit RP</div>';
        jausCreateButtonParent.appendChild(jausNode);
        let jausContextButton = document.getElementById("jaus-berp-main-button");
        jausContextButton.addEventListener("click", toggleJAUSBox);
        window.JAUS = {};
        window.JAUS.isCloud = true;
        window.JAUS.userFound = false;
        window.JAUS.restAPIJira = window.location.href.split(/issues/).shift() + 'rest/api/2/';
        window.JAUS.restAPIJiraSD = window.location.href.split(/issues/).shift() + 'rest/servicedeskapi/request/';
        window.JAUS.issues = {};
    } else if (jausCreateButtonDC) {
        GM_addStyle(jausCSS);
        GM_addStyle(jausSpinnerCSS);
        let jausCreateButtonParent = jausCreateButtonDC.parentNode.parentNode;
        var jausNode = document.createElement("li");
        jausNode.id = 'jaus-berp-main-menu';
        //aui-button aui-button-primary aui-style
        jausNode.innerHTML = '<div class="jaus jaus-main-button" id="jaus-berp-main-button" accessKey="u" title="' + jausUserscriptVersion + '">Bulk Edit RP</div>';
        jausCreateButtonParent.appendChild(jausNode);
        let jausContextButton = document.getElementById("jaus-berp-main-button");
        jausContextButton.addEventListener("click", toggleJAUSBox);
        window.JAUS = {};
        window.JAUS.isCloud = false;
        window.JAUS.userFound = false;
        window.JAUS.restAPIJira = window.location.href.split(/issues/).shift() + 'rest/api/2/';
        window.JAUS.restAPIJiraSD = window.location.href.split(/issues/).shift() + 'rest/servicedeskapi/request/';
        window.JAUS.issues = {};
    }
}, 1000);
