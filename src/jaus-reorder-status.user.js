// ==UserScript==
// @name        JAUS Reorder Status
// @namespace   JAUS
// @version     1.1.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez)
// @description Shift Statuses up or down the list by N positions
// @homepage    https://jaus.atlassian.net/wiki/spaces/JAUS/pages/33452/Reorder+Status
// @supportURL  https://jaus.atlassian.net/servicedesk/customer/portals
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// @match       *://*/*/ViewStatuses.jspa*
// @grant       GM_xmlhttpRequest
// @grant       GM_addStyle
// @downloadURL https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-reorder-status.user.js
// @updateURL   https://bitbucket.org/jira-admin-userscripts/jaus/raw/master/src/jaus-reorder-status.user.js
// ==/UserScript==
let jausUserscriptVersion = 'v1.1.0'; // Always keep this in sync with the @version above

/*
 * Spinner from CSS from w3schools.com/howto/howto_css_loader.asp
 */
var jausSpinnerCSS = `
.jausSpinnerClass {
    visibility: hidden;
    margin-left: auto;
    margin-right: auto;
    margin-top: 0px;
    position: center;
    border: 4px solid #DFE1E6;
    border-top: 4px solid #0052CC;
    border-radius: 50%;
    width: 24px;
    height: 24px;
    animation: spin 2s linear infinite;
    text-align: center;
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}`;

var jausCSS = `
.jaus {
    all: none;
    font-size: 14px;
    font-family: "Segoe UI", Helvetica, sans-serif, monospace;
}

.jaus-main-button {
    color: #555555;
    font-weight: bold;
    text-align: center;
    margin-left: 30px;
    border-style: solid;
    border-width: 1px;
    border-radius: 5px;
    border-color: GoldenRod;
    background-color: gold;
    padding: 4px 10px;
    cursor: pointer;
    transition-duration: 0.1s;
    transition-property: background-color;
    transition-timing-function: ease-out;
}

.jaus-main-button:hover {
    background-color: #ffe033;
}

.jaus-blanket {
    background-color: rgba(23, 43, 77, 0.45);
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 2500;
    left: 0px;
    top: 0px;
}

.jaus-box {
    top: 100px;
    right: 0;
    bottom: 100px;
    left: 0;
    margin: auto;
    position: fixed;
    display: block;
    background-color: white;
    z-index: 3000;
    width: 35%;
    height: 370px;
    max-height: 50%;
    min-height: 300px;
    border-style: solid;
    border-color: white;
    border-radius: 5px;
    font-family: "Segoe UI", Helvetica, sans-serif, monospace;
}

.jaus-box header {
    position: sticky;
    height: 60px;
    display: flex;
    border-bottom-style: solid;
    border-bottom-width: 2px;
    border-bottom-color: #ebecf0;
}

.jaus-box header h1 {
    margin-left: 0px;
    text-align: left;
    font-size: 20px;
    position: sticky;
    color: rgb(23, 43, 77);
    padding: 10px 10px 10px 20px;
}

.jaus-box header nav {
    position: absolute;
    margin-left: auto;
    padding: 5px;
    right: 0px;
}

.jaus-box article {
    display: block;
    position: absolute;
    top: 62px;
    right: 0px;
    bottom: 52px;
    left: 0px;
    overflow: auto;
    padding: 30px;
}

.jaus-box article p {
    color: rgb(107, 119, 140);
    font-size: 14px;
    line-height: 20px;
    display: inline-block;
}

.jaus-box article label {
    font-weight: 500;
    margin-left: auto;
    margin-right: auto;
    width: 150px;
    text-align: right;
    display: inline-grid;
    padding: 3px 20px 3px 4px;
}

.jaus-box footer {
    color: #555555;
    position: absolute;
    display: flex;
    bottom: 0px;
    right: 0px;
    left: 0px;
    height: 50px;
    border-top-style: solid;
    border-top-width: 2px;
    border-top-color: #ebecf0;
}

.jaus-box footer p {
    position: absolute;
    right: 0px;
    bottom: 0px;
    padding: 5px;
    font-style: italic;
    font-family: monospace, sans-serif, v, "Segoe UI";
    display: inline-grid;
}

.jaus-text-field {
    background-color: rgb(250, 251, 252);
    border-style: solid;
    border-width: 2px;
    border-color: rgb(223, 225, 230);
    font-size: 14px;
    padding: 3px 4px;
    width: 300px;
    line-height: 20px;
    border-radius: 3px;
    color: rgb(23, 43, 77);
    font-weight: 400;
    display: inline-grid;
}

.jaus-select-field {
    background-color: rgb(235, 236, 240);
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill='%23344563' d='M6.744 8.744a1.053 1.053 0 000 1.49l4.547 4.557a1 1 0 001.416 0l4.55-4.558a1.051 1.051 0 10-1.488-1.488l-3.77 3.776-3.768-3.776a1.051 1.051 0 00-1.487 0z'/%3E%3C/svg%3E");
    background-position: 100% 50%;
    background-repeat: no-repeat;
    border-style: solid;
    border-width: 2px;
    border-color: rgb(235, 236, 240);
    display: inline-grid;
    appearance: none;
    border-radius: 3px;
    font-weight: 400;
    font-size: 14px;
    color: rgb(23, 43, 77);
    height: 30px;
    padding: 3px 0px 3px 5px;
    width: 312px;
}

.jaus-rs-lower-section {
    border-top-style: solid;
    border-top-width: 2px;
    border-top-color: #ebecf0;
    margin-top: 10px;
    padding-top: 15px;
    width: 100%;
    position: relative;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
}

#jaus-rs-commit-button {
    margin-top: 10px;
    margin-left: 5px;
    padding: 3px 5px 3px 5px;
}
`;

function jausPrintVersion() {
    return '<a href="https://jaus.atlassian.net/wiki/spaces/JAUS/pages/33452/Reorder+Status" target="_blank">' + jausUserscriptVersion + '&nearr;</a>';
}

function toggleJAUSBox() {
    let jausBox = document.getElementById('jaus-rs-box');
    let jausBlanket = document.getElementById('jaus-rs-blanket');
    let boxRemoved = false;
    if (jausBox) {
        jausBox.parentNode.removeChild(jausBox);
        boxRemoved = true;
    }
    if (jausBlanket) {
        jausBlanket.parentNode.removeChild(jausBlanket);
        boxRemoved = true;
    }
    if (!boxRemoved) {
        let jausBlanket = document.createElement("div");
        jausBlanket.id = "jaus-rs-blanket";
        jausBlanket.className = "jaus jaus-blanket";
        document.body.appendChild(jausBlanket);
        jausBlanket.addEventListener("click", toggleJAUSBox);

        jausBox = document.createElement("section");
        jausBox.id = 'jaus-rs-box';
        jausBox.className = 'jaus jaus-box';
        jausBox.innerHTML = '<header id="jaus-rs-box-header"><h1>JAUS - Reorder Status</h1><nav id="jaus-rs-nav"><a href="#" id="jaus-rs-close-button">Close</a></nav></header><article id="jaus-rs-content"><section>' +
            '<form id="jaus-rs-form" method="POST" action=""><fieldset>' +
            '<p><label for="jaus-rs-status-id">Status Id</label><input class="jaus jaus-text-field" type="number" name="jaus-rs-status-id" id="jaus-rs-status-id" value="" placeholder="The status Id (hover over the edit links)" required></input></p>' +
            '<p><label for="jaus-rs-direction">Direction to move </label><select class="jaus jaus-select-field" id="jaus-rs-direction" required><option value="up" selected>Upwards</option><option value="down">Downwards</option></select></p>' +
            '<p><label for="jaus-rs-positions">Number of positions </label><input class="jaus jaus-text-field" type="number" name="jaus-rs-positions" id="jaus-rs-positions" value="" placeholder="The number of positions to shift" required></input>' +
            '<button type="button" id="jaus-rs-commit-button">Commit!</button></p>' +
            '</fieldset></form></section>' +
            '<section id="jaus-rs-lower-section" class="jaus jaus-rs-lower-section">' +
            '<aside id="jaus-rs-commit-feedback" class="jaus jaus-rs-commit-feedback">' +
            '<div class="jaus jausSpinnerClass" id="jaus-rs-spinner"></div>' +
            '</aside>' +
            '</section>' +
            '</article><footer id="jaus-box-footer"><p>' + jausPrintVersion() + '</p></footer>';
        document.body.appendChild(jausBox);
        document.getElementById('jaus-rs-close-button').addEventListener("click", toggleJAUSBox);
        document.getElementById('jaus-rs-commit-button').addEventListener("click", jausReorderStatus);
    }
}

function jausReorderStatus() {
    if (!document.getElementById('jaus-rs-status-id').value || !document.getElementById('jaus-rs-positions').value) {
        alert("hi");
        return false;
    }
    document.getElementById('jaus-rs-commit-button').style.visibility = "hidden";
    document.getElementById('jaus-rs-spinner').style.visibility = "visible";
    let jausAtlassianToken = document.getElementById("atlassian-token").content;
    let shift = document.getElementById("jaus-rs-positions").value;
    let statusURL = window.location.href.split(/ViewStatuses/).shift();
    if (document.getElementById('jaus-rs-direction').value == 'up') {
        statusURL += 'StatusUp.jspa?up=';
    } else {
        statusURL += 'StatusDown.jspa?down=';
    }
    statusURL += document.getElementById("jaus-rs-status-id").value + '&atl_token=' + jausAtlassianToken;
    let index = shift;
    jausReorderStatusRequest(statusURL, index);
    setTimeout(function () {
        location.reload()
    }, jausFormSubmitDelay)
}

function jausReorderStatusRequest(statusURL, index) {
    if (index > 0) {
        GM_xmlhttpRequest({
            method: 'GET',
            headers: {"Content-Type": "text/plain"},
            url: statusURL,
            responseType: 'text/plain',
            onload: function (responseReorderStatus) {
                jausReorderStatusRequest(statusURL, index - 1);
            }
        });
    } else {
        document.getElementById('jaus-rs-form').reset();
        //document.getElementById('jaus-rs-form').submit();
        location.reload();
    }
}

/*
 * "MAIN" BODY:
 * Injects a button into the top menu.
 */
setTimeout(function () {
    let jausCreateButtonCloud = document.getElementById('createGlobalItem');
    let jausCreateButtonDC = document.getElementById('create_link');
    if (jausCreateButtonCloud) {
        GM_addStyle(jausCSS);
        GM_addStyle(jausSpinnerCSS);
        let jausCreateButtonParent = jausCreateButtonCloud.parentNode;
        var jausNode = document.createElement("div");
        jausNode.id = 'jaus-rs-main-menu';
        jausNode.className = jausCreateButtonParent.className;
        jausNode.innerHTML = '<div class="jaus jaus-main-button" id="jaus-rs-main-button" accessKey="u" title="' + jausUserscriptVersion + '">Reorder Status</div>';
        jausCreateButtonParent.appendChild(jausNode);
        let jausContextButton = document.getElementById("jaus-rs-main-button");
        jausContextButton.addEventListener("click", toggleJAUSBox);
        window.JAUS = {};
        window.JAUS.isCloud = true;
    } else if (jausCreateButtonDC) {
        GM_addStyle(jausCSS);
        GM_addStyle(jausSpinnerCSS);
        let jausCreateButtonParent = jausCreateButtonDC.parentNode.parentNode;
        var jausNode = document.createElement("li");
        jausNode.id = 'jaus-rs-main-menu';
        jausNode.innerHTML = '<div class="jaus jaus-main-button" id="jaus-rs-main-button" accessKey="u" title="' + jausUserscriptVersion + '">Reorder Status</div>';
        jausCreateButtonParent.appendChild(jausNode);
        let jausContextButton = document.getElementById("jaus-rs-main-button");
        jausContextButton.addEventListener("click", toggleJAUSBox);
        window.JAUS = {};
        window.JAUS.isCloud = false;
    }
}, 1000);
