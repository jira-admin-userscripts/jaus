# JAUS - Jira Admin UserScripts #

This repository contains Userscripts that Jira Admins may find useful on their daily activities.
These scripts compose what we call JAUS: Jira Admin UserScripts.
Please visit the [JAUS site](https://jaus.atlassian.net/wiki/spaces/JAUS/overview) for more information!

JAUS and each individual scripts are licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0).
JAUS itself was designed by [Rodrigo Martinez](https://www.linkedin.com/in/rodrigo-carvalho-martinez). Individual Userscripts should be credited to the corresponding author(s).
If sharing, editing or modifying any of these scripts, be sure to mention the respective author and provide a link to [jaus.atlassian.net](https://jaus.atlassian.net).


### What are Userscripts ###
Userscripts are Javascript code that's executed in the user's browser when browsing specific pages or web applications screens.

They implement what's called Augmented Browsing and are commonly used to enrich the user's browsing experience of automate some tasks in web apps.


### Setting up your browser ###

First you need to install a browser Extension called "Userscript Manager".
Refer to [Getting started with JAUS](https://jaus.atlassian.net/wiki/spaces/JAUS/pages/393370/Get+Started+with+JAUS) for instructions on how to install and use the Userscripts!


### How to contribute ###

There are several qways you can contribute to JAUS:

* Notify of issues or problems
* Suggest improvements to existing Userscripts
* Suggest ideas for new Userscripts that may help fellow Jira Admins

All this can be done through the [JAUS Help Center](https://jaus.atlassian.net/servicedesk)!
